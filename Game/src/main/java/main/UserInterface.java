package main;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import objects.Health;
import objects.Items;

/**
 * The UserInterface class handles the graphical user interface (GUI) elements of the game,
 * including health indicators, messages, dialogs, and inventory.
 */
public class UserInterface {

    // Main
    protected GameController gameController;
    protected AnchorPane anchorPane;

    // Health
    protected ImageView[] hearts;

    // Items messages
    protected boolean messageOn;
    protected String message;
    protected int messageTimer;

    // Dialog
    protected String dialogString;

    // Inventory
    protected int slotCol;
    protected int slotRow;
    protected String descriptionString;
    protected Label descriptionLabel;

    public UserInterface(GameController gameController) {
        this.gameController = gameController;
        anchorPane = gameController.getUIAnchorPane();

        // Hearts
        hearts = new Health(6).getHeartImages(); //here can be any number

        // Items messages
        messageOn = false;
        message = null;
        messageTimer = 0;

        // Inventory
        slotCol = 0;
        slotRow = 0;

        // Description label
        setUpDescriptionLabel();
    }

    /**
     * Sets up the description label for inventory items.
     */
    private void setUpDescriptionLabel() {
        descriptionLabel = new Label();
        descriptionLabel.setWrapText(true);
        descriptionLabel.setStyle("-fx-background-color: rgba(0, 0, 0, 0.8); -fx-text-fill: white;");
        descriptionLabel.setPrefSize((double) GameController.tileSize * 4 - 28, (double) GameController.tileSize * 3 - 28);
    }

    /**
     * displays a message on the screen.
     *
     * @param txt the message to display
     */
    public void showMessage(String txt) {
        message = txt;
        messageOn = true;
    }

    /**
     * draws all UI elements based on the current game state.
     */
    public void draw() {
        drawHearts();
        switch (gameController.gameState) {
            case States.PLAY_STATE:
                drawText();
                break;
            case States.PAUSE_STATE:
                drawPause();
                break;
            case States.DIALOG_STATE:
                drawDialogWindow();
                break;
            case States.INVENTORY_STATE:
                drawInventory();
                break;
        }
    }

    /**
     * draws the player's health as hearts on the screen.
     */
    private void drawHearts() {
        int heartX = GameController.tileSize / 2;
        int heartY = GameController.tileSize / 2;
        int i = 0;
        // Blank hearts
        while (i < gameController.hero.getHealth().getMaxHealth() / 2) {
            addItemToRoot(heartX, heartY, hearts[2], anchorPane, 32, 32);
            i++;
            heartX += GameController.tileSize;
        }
        // Reset
        heartX = GameController.tileSize / 2;
        i = 0;
        // Draw current life
        while (i < gameController.hero.getHealth().getCurrentHealth()) {
            addItemToRoot(heartX, heartY, hearts[1], anchorPane, 32, 32);
            i++;
            if (i < gameController.hero.getHealth().getCurrentHealth()) {
                addItemToRoot(heartX, heartY, hearts[0], anchorPane, 32, 32);
            }
            i++;
            heartX += GameController.tileSize;
        }
    }

    /**
     * draws the pause screen.
     */
    public void drawPause() {
        Label pause = new Label("Pause");
        pause.setStyle("-fx-text-fill: white; -fx-font-size: 40; -fx-background-color: rgba(0, 0, 0, 0);");
        AnchorPane.setTopAnchor(pause, (double) GameController.maxScreenHeight / 2);
        AnchorPane.setLeftAnchor(pause, (double) GameController.maxScreenWidth / 2);
        anchorPane.getChildren().add(pause);
    }

    /**
     * draws the dialog window.
     */
    public void drawDialogWindow() {
        int windowX = GameController.tileSize;
        int windowY = GameController.tileSize / 2;
        int windowWidth = GameController.maxScreenWidth - GameController.tileSize * 2;
        int windowHeight = GameController.tileSize * 3;

        Label dialogTextLabel = new Label(dialogString);
        dialogTextLabel.setWrapText(true);
        dialogTextLabel.setStyle("-fx-background-color: rgba(0, 0, 0, 0.8); -fx-text-fill: white;");
        dialogTextLabel.setPrefSize(windowWidth, windowHeight);

        AnchorPane.setTopAnchor(dialogTextLabel, (double) windowY);
        AnchorPane.setLeftAnchor(dialogTextLabel, (double) windowX);
        anchorPane.getChildren().add(dialogTextLabel);
    }

    /**
     * draws the inventory screen.
     */
    public void drawInventory() {
        int windowX = GameController.tileSize;
        int windowY = GameController.tileSize * 2;
        int windowWidth = GameController.tileSize * 4 - 28;
        int windowHeight = GameController.tileSize * 3 - 28;

        int descriptionX = windowX + windowWidth + 30;
        int descriptionY = windowY;

        anchorPane.getChildren().clear();

        Pane inventoryPane = new Pane();
        inventoryPane.setPrefSize(windowWidth, windowHeight);
        inventoryPane.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5); -fx-border-color: white; -fx-border-width: 2;");

        final int slotXStart = 10;
        final int slotYStart = 10;
        final int slotSize = GameController.tileSize;
        final int slotsInRow = 3;

        for (int i = 0; i < gameController.hero.getInventory().size(); i++) {
            if (gameController.hero.getInventory().get(i) != null) {
                int col = i % slotsInRow;
                int row = i / slotsInRow;
                int slotX = slotXStart + col * slotSize;
                int slotY = slotYStart + row * slotSize;
                addItemToRoot(slotX, slotY, gameController.hero.getInventory().get(i).getImageView(), inventoryPane,
                        slotSize, slotSize);
            }
        }

        int cursorX = slotXStart + slotCol * slotSize;
        int cursorY = slotYStart + slotRow * slotSize;
        int cursorWidth = slotSize;
        int cursorHeight = slotSize;

        Rectangle cursor = new Rectangle(cursorWidth, cursorHeight);
        cursor.setFill(Color.TRANSPARENT);
        cursor.setStroke(Color.WHITE);
        cursor.setStrokeWidth(2);
        cursor.setLayoutX(cursorX);
        cursor.setLayoutY(cursorY);

        inventoryPane.getChildren().add(cursor);

        AnchorPane.setTopAnchor(inventoryPane, (double) windowY);
        AnchorPane.setLeftAnchor(inventoryPane, (double) windowX);
        anchorPane.getChildren().add(inventoryPane);

        AnchorPane.setTopAnchor(descriptionLabel, (double) descriptionY);
        AnchorPane.setLeftAnchor(descriptionLabel, (double) descriptionX);
        anchorPane.getChildren().add(descriptionLabel);

        updateDescription();
    }

    /**
     * updates the description of the selected inventory item.
     */
    private void updateDescription() {
        int index = slotRow * 3 + slotCol;
        if (index < gameController.hero.getInventory().size() && gameController.hero.getInventory().get(index) != null) {
            Items item = gameController.hero.getInventory().get(index);
            String formattedDescription = "Item: " + item.getName() + "\n\n" + item.getDescription();
            descriptionLabel.setText(formattedDescription);
        } else {
            descriptionLabel.setText("");
        }
    }

    /**
     * draws text messages on the screen.
     */
    public void drawText() {
        if (messageOn) {
            Label label = new Label(message);
            label.setStyle("-fx-text-fill: white; -fx-font-size: 40; -fx-background-color: rgba(0, 0, 0, 0.5);");
            anchorPane.getChildren().add(label);
            AnchorPane.setTopAnchor(label, (double) gameController.getHero().screenX - GameController.tileSize * 5);
            AnchorPane.setLeftAnchor(label, (double) gameController.getHero().screenY - GameController.tileSize * 5);
            messageTimer++;

            if (messageTimer > 100) {
                messageTimer = 0;
                messageOn = false;
            }
        }
    }

    /**
     * Adds an item to the root pane at the specified position.
     */
    private void addItemToRoot(int x, int y, ImageView imageView, Pane root, int width, int height) {
        ImageView imageViewTmp = new ImageView(imageView.getImage());
        imageViewTmp.setLayoutX(x);
        imageViewTmp.setLayoutY(y);
        imageViewTmp.setFitHeight(height);
        imageViewTmp.setFitWidth(width);
        root.getChildren().add(imageViewTmp);
    }

    // Setters

    public void setDialogString(String dialogString) {
        this.dialogString = dialogString;
    }
}
