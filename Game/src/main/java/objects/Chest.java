package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class Chest extends Items {

    public Chest() {
        name = "Chest";
        try {
            collision = true;
            imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                    getResourceAsStream("/items/chest.png"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
