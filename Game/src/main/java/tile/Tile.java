package tile;

import javafx.scene.image.ImageView;

public class Tile {
    protected boolean collision;
    protected ImageView imageView;
    public Tile()
    {
        collision = false;
    }

    // getters

    public boolean isCollision() {
        return collision;
    }
}
