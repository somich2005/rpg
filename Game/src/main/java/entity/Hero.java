package entity;

import javafx.application.Platform;
import main.*;
import objects.*;

import java.util.ArrayList;
import java.util.List;

public class Hero extends Entity {

    // position
    public final int screenX;
    public final int screenY;

    // game properties
    protected String name;
    protected ArrayList<Items> inventory;

    protected Key key;
    protected Boots boots;
    protected HealPoison healPoison;

    // main
    protected boolean invincible;
    protected int invincibleCounter;
    protected UserInterface userInterface;

    public Hero(String name, int positionX, int positionY, GameController gameController) {

        // position
        super(positionX, positionY, gameController);
        screenX = GameController.maxScreenWidth / 2 - (GameController.tileSize / 2);
        screenY = GameController.maxScreenHeight / 2 - (GameController.tileSize / 2);

        // game properties
        this.name = name;
        this.speed = 4;

        damage = 1;
        health = new Health(6);
        inventory = new ArrayList<>();
        key = new Key();

        // collision
        solidAreaDefaultX = (int) solidArea.getX();
        solidAreaDefaultY = (int) solidArea.getY();

        // main
        collisionOn = false;
        invincible = false;
        invincibleCounter = 0;
        userInterface = gameController.getUserInterface();
    }

    // update only tha hero animation in case !fullUpdate
    public void updateHero(KeyController keyController, boolean fullUpdate) {

        if (health.getCurrentHealth() == 0)
            die();

        if (keyController.isWPressed()) {
            spriteAnimation.heroAction = Actions.UP;
        } else if (keyController.isSPressed()) {
            spriteAnimation.heroAction = Actions.DOWN;
        } else if (keyController.isDPressed()) {
            spriteAnimation.heroDirection = "right";
            spriteAnimation.heroAction = Actions.RIGHT;
        } else if (keyController.isAPressed()) {
            spriteAnimation.heroAction = Actions.LEFT;
            spriteAnimation.heroDirection = "left";
        } else {
            spriteAnimation.heroAction = Actions.IDLE;
        }

        if (fullUpdate) {
            // tile collision
            collisionOn = false;
            gameController.getCollisionController().checkTile(this, spriteAnimation.heroAction);
            // object collision
            pickUpObject(gameController.getCollisionController().checkObject(this, true));
            // npc collision
            interactNPC(gameController.getCollisionController().checkEntity(this, gameController.getNpcElf(), false));
            // monster collision
            for (int i = 0; i < gameController.getMonsters().length; i++) {
                if (gameController.getMonsters()[i] != null) {
                    int index = gameController.getCollisionController().checkEntity(this,
                            gameController.getMonsters()[i], gameController.getMonsters()[i].isDead);
                    contactMonster(index, gameController.getMonsters()[i]);
                }
            }
        }

        if (!collisionOn) {
            switch (spriteAnimation.heroAction) {
                case Actions.UP:
                    moveUp();
                    break;
                case Actions.DOWN:
                    moveDown();
                    break;
                case Actions.LEFT:
                    moveLeft();
                    break;
                case Actions.RIGHT:
                    moveRight();
                    break;
            }
        }
        updateAnimation();
    }

    private void updateAnimation() {  // update every 5 frames. Otherwise, it looks terrible
        spriteAnimation.spriteCounter++;

        if (spriteAnimation.spriteCounter > 5) {
            spriteAnimation.spriteMoveNumber++;
            if (spriteAnimation.spriteMoveNumber == 4)
                spriteAnimation.spriteMoveNumber = 0;
            spriteAnimation.spriteCounter = 0;
        }

        if (invincible) { // hero can`t take damage withing 60 frames
            invincibleCounter++;
            if (invincibleCounter > 60) {
                invincible = false;
                invincibleCounter = 0;
            }
        }
    }

    public void contactMonster(int index, Monster monster) {
        if (index != -1) { // index -1 when no contact
            if (!invincible) {
                health.setCurrentHealth(health.getCurrentHealth() - 1);
                invincible = true;
            }
            attackMonster(monster);
        }
    }

    private void attackMonster(Monster monster) {
        if (!monster.invincible) {
            if (monster.health.getCurrentHealth() > 0) {
                monster.health.setCurrentHealth(monster.health.getCurrentHealth() - damage);
                gameController.playClips(Sounds.GET_DAMAGE.getValue());
                monster.invincible = true;
            } else {
                monster.die();
            }
        }
    }

    protected void pickUpObject(int i) {
        Sword sword;
        if (i >= 0) {
            String itemName = gameController.getItems()[i].getName();

            switch (itemName) {
                case "Sword":
                    sword = (Sword) gameController.getItems()[i];
                    inventory.add(sword);
                    damage += sword.getAdditionalDamage();
                    gameController.getItems()[i] = null;
                    gameController.playClips(Sounds.PICK_UP_SWORD.getValue());
                    userInterface.showMessage("Your sword: " + sword.getName());
                    break;
                case "Door":
                    if (key.getAmount() > 0) {
                        gameController.getItems()[i] = null;
                        gameController.playClips(Sounds.DOOR_OPENING.getValue());
                        key.setAmount(key.getAmount() - 1);
                        inventory.remove(key);
                        userInterface.showMessage("KNOCK-KNOCK");
                    }
                    break;
                case "Key":
                    if (key == null)
                        key = (Key) gameController.getItems()[i];
                    key.setAmount(key.getAmount() + 1);
                    inventory.add(key);
                    gameController.getItems()[i] = null;
                    gameController.playClips(Sounds.PICK_UP_KEY.getValue());
                    userInterface.showMessage("You got a key!");
                    break;
                case "Boots":
                    boots = (Boots) gameController.getItems()[i];
                    speed += 2;
                    inventory.add(boots);
                    gameController.getItems()[i] = null;
                    gameController.playClips(Sounds.PUT_ON_BOOTS.getValue());
                    userInterface.showMessage("Speed: +2");
                    break;
                case "Heal poison":
                    if (health.getCurrentHealth() < health.getMaxHealth()) {
                        healPoison = (HealPoison) gameController.getItems()[i];
                        health.setCurrentHealth(health.getCurrentHealth() + healPoison.getHealing());
                        gameController.playClips(Sounds.POISON.getValue());
                        gameController.getItems()[i] = null;
                    }
                    break;
                case "Chest":
                    if (key.getAmount() > 0) {
                        gameController.getItems()[i] = null;
                        gameController.playClips(Sounds.CHEST.getValue());
                        win();
                    }
            }
        }
    }

    private void interactNPC(int i) {
        if (i > 0) {
            gameController.setGameState(States.DIALOG_STATE);
            Platform.runLater(() -> gameController.getNpcElf().say());
        }
    }

    @Override
    protected void die() {
        speed = 0;
        isDead = true;
        gameController.showScreen("Game over");
    }

    private void win() {
        speed = 0;
        gameController.showScreen("Victory");
    }


    public List<Items> getInventory() {
        return inventory;
    }

    // method to set the position of the hero
    @Override
    public void setWorldX(int x) {
        this.worldX = x * GameController.tileSize;
    }

    @Override
    public void setWorldY(int y) {
        this.worldY = y * GameController.tileSize;
    }

}
