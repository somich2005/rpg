package main;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MenuController {

    @FXML
    private Button exitButton;

    @FXML
    private Button startButton;

    @FXML
    void exitGame(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void startGame(ActionEvent event) {
        Game game = new Game();
        game.show();
    }

}
