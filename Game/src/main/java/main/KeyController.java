package main;

import javafx.scene.input.KeyCode;

import java.util.logging.Logger;

/**
 * The KeyController class handles keyboard input for the game.
 * It updates the game state and entity actions based on the keys pressed.
 */
public class KeyController {

    private static final Logger logger = Logger.getLogger(KeyController.class.getName());

    // Keys
    protected boolean WPressed;
    protected boolean SPressed;
    protected boolean APressed;
    protected boolean DPressed;
    protected boolean PPressed;
    protected boolean IPressed;

    // Main
    protected GameController gameController;


    public KeyController(GameController gameController) {
        this.gameController = gameController;
    }

    /**
     * Handles key pressed events and updates the game state and entity actions accordingly.
     *
     * @param key the KeyCode of the pressed key
     */
    public void handleKeyPressed(KeyCode key) {
        switch (gameController.gameState) {
            case PLAY_STATE:
                if (key == KeyCode.W) {
                    WPressed = true;
                } else if (key == KeyCode.S) {
                    SPressed = true;
                } else if (key == KeyCode.A) {
                    APressed = true;
                } else if (key == KeyCode.D) {
                    DPressed = true;
                } else if (key == KeyCode.P) {
                    logger.info("Current game state - pause");
                    gameController.gameState = States.PAUSE_STATE;
                    PPressed = true;
                } else if (key == KeyCode.I) {
                    logger.info("Current game state - inventory");
                    gameController.gameState = States.INVENTORY_STATE;
                    IPressed = true;
                }
                break;
            case PAUSE_STATE:
                if (key == KeyCode.P) {
                    logger.info("Current game state - play");
                    gameController.gameState = States.PLAY_STATE;
                    PPressed = false;
                }
                break;
            case DIALOG_STATE:
                if (key == KeyCode.ENTER) {
                    logger.info("Current game state - play");
                    gameController.gameState = States.PLAY_STATE;
                }
                break;
            case INVENTORY_STATE:
                if (key == KeyCode.I) {
                    logger.info("Current game state - play");
                    gameController.gameState = States.PLAY_STATE;
                    IPressed = false;
                } else if (key == KeyCode.W && gameController.userInterface.slotRow != 0) {
                    gameController.userInterface.slotRow--;
                } else if (key == KeyCode.S && gameController.userInterface.slotRow != 1) {
                    gameController.userInterface.slotRow++;
                } else if (key == KeyCode.A && gameController.userInterface.slotCol != 0) {
                    gameController.userInterface.slotCol--;
                } else if (key == KeyCode.D && gameController.userInterface.slotCol != 2) {
                    gameController.userInterface.slotCol++;
                }
                break;
        }
    }

    /**
     * Handles key released events and updates the entity actions accordingly.
     *
     * @param key the KeyCode of the released key
     */
    public void handleKeyReleased(KeyCode key) {
        if (key == KeyCode.W) {
            WPressed = false;
        } else if (key == KeyCode.S) {
            SPressed = false;
        } else if (key == KeyCode.A) {
            APressed = false;
        } else if (key == KeyCode.D) {
            DPressed = false;
        }
    }

    // Getters


    public boolean isWPressed() {
        return WPressed;
    }


    public boolean isSPressed() {
        return SPressed;
    }


    public boolean isAPressed() {
        return APressed;
    }


    public boolean isDPressed() {
        return DPressed;
    }
}
