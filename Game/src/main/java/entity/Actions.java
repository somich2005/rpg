package entity;

public enum Actions {
    RIGHT("go right"),
    LEFT("go left"),
    DOWN("go down"),
    UP("go up"),
    IDLE("idle");

    private final String direction;

    Actions(String value) {
        this.direction = value;
    }

    public String getDirection() {
        return this.direction;
    }
}
