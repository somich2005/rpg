package main;

import entity.Monster;
import entity.NPCElf;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import entity.Hero;
import objects.Items;
import tile.TileController;

import java.util.Arrays;
import java.util.logging.Logger;

/**
 * The GameController class manages the main game loop and game state.
 * It handles the initialization and updating of game objects, entities, and UI components.
 */
public class GameController implements Runnable {

    private static final Logger logger = Logger.getLogger(GameController.class.getName());

    // Screen settings
    public static final int originalTileSize = 16; // 16x16
    protected static final int scale = 3;
    public static final int maxScreenCol = 16;
    public static final int maxScreenRow = 12;
    public static final int tileSize = originalTileSize * scale; // 48x48
    public static final int maxScreenWidth = tileSize * maxScreenCol;
    public static final int maxScreenHeight = tileSize * maxScreenRow;

    // World Settings
    public static final int maxWorldCol = 30;
    public static final int maxWorldRow = 25;

    // FPS
    protected final int FPS;
    protected final double drawInterval;

    // Main
    protected AnchorPane root;
    protected AnchorPane mapAnchorPane;
    protected AnchorPane itemsAnchorPane;
    protected AnchorPane entityAnchorPane;
    protected AnchorPane UIAnchorPane;
    protected CollisionController collisionController;
    protected Thread gameThread;
    protected Stage stage;
    protected Scene scene;
    protected KeyController keyController;
    protected Sound sound;
    protected UserInterface userInterface;

    // Entity
    protected Hero hero;
    protected NPCElf npcElf;
    Monster[] monsters;

    // Objects
    protected TileController tileController;
    protected Items[] items;
    protected SetUpController setUpController;

    // Game state
    protected States gameState;

    /**
     * Default constructor for tests. Initializes FPS and draw interval.
     */
    public GameController() {
        FPS = 60;
        drawInterval = ((double) 1000000000) / FPS;
        items = new Items[10];
    }

    /**
     * Constructor for initializing the game with the given root pane, stage, and scene.
     *
     * @param root  the root AnchorPane
     * @param stage the primary stage
     * @param scene the main scene
     */
    public GameController(AnchorPane root, Stage stage, Scene scene) {

        FPS = 60;
        drawInterval = ((double) 1000000000) / FPS;

        items = new Items[10];
        setUpController = new SetUpController(this);

        this.root = root;
        this.stage = stage;
        this.scene = scene;
        mapAnchorPane = new AnchorPane();
        itemsAnchorPane = new AnchorPane();
        entityAnchorPane = new AnchorPane();
        UIAnchorPane = new AnchorPane();
        keyController = new KeyController(this);
        scene.setOnKeyPressed(e -> keyController.handleKeyPressed(e.getCode()));
        scene.setOnKeyReleased(e -> keyController.handleKeyReleased(e.getCode()));
        sound = new Sound();
        userInterface = new UserInterface(this);
        addAnchorPanes();
        gameState = States.PLAY_STATE;

        hero = new Hero("Hero", tileSize * 4, tileSize * 4, this);
        npcElf = new NPCElf(GameController.tileSize * 2, GameController.tileSize * 2, this);
        monsters = new Monster[10];

        tileController = new TileController(this);
        collisionController = new CollisionController(this);
    }

    /**
     * Starts the game thread.
     */
    protected void startGameThread() {
        logger.info("Game threat started");
        gameThread = new Thread(this);
        gameThread.start();
    }


    /**
     * The main game loop. Updates and repaints components at the specified FPS.
     * The loop is taken from the tutorial
     * <a href="https://www.youtube.com/watch?v=VpH33Uw-_0E&list=PL_QPQmz5C6WUF-pOQDsbsKbaBZqXj4qSq&index=2&ab_channel=RyiSnow">...</a>
     */
    @Override
    public void run() {
        double nextDrawInterval = System.nanoTime() + drawInterval;
        while (gameThread != null) {
            update();
            paintComponent();
            try {
                double remainingTime = nextDrawInterval - System.nanoTime();
                remainingTime /= 1000000; // Thread.sleep() method accept only ms, but remainingTime is in nanoseconds
                if (remainingTime < 0) // It is a loop, so variable can get values > Double.max and become < 0
                    remainingTime = 0;
                Thread.sleep((long) remainingTime); // cast to long
                nextDrawInterval += drawInterval;
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Updates all dependencies in the game based on the current game state.
     */
    protected void update() {
        switch (gameState) {
            case States.PLAY_STATE:
                hero.updateHero(keyController, true);
                if (npcElf != null) // npc located only on the first map. After transition npc is null
                    npcElf.updateNPC();
                checkForMapTransition();
                for (Monster monster : monsters) {
                    if (monster != null) {
                        monster.update();
                    }
                }
                if (sound.isPaused()) {
                    sound.resume();
                }
                break;
            case States.PAUSE_STATE:
                if (!sound.isPaused()) {
                    sound.pause();
                }
                break;
            case States.DIALOG_STATE:
                hero.updateHero(keyController, false);
                npcElf.updateNPC();
                break;
            case States.INVENTORY_STATE:
                hero.updateHero(keyController, true);
        }
    }

    /**
     * Checks for map transition conditions and switches the map if necessary.
     */
    private void checkForMapTransition() {
        if ((hero.getWorldX() >= GameController.tileSize * 14 && hero.getWorldX() <= GameController.tileSize * 15)
                && (hero.getWorldY() >= 0 && hero.getWorldY() <= GameController.tileSize)
                && tileController.getCurrentMap() == 1) {
            switchMap();
        }
    }

    /**
     * Switches the map and resets game objects.
     */
    private void switchMap() {
        logger.info("map changed");
        tileController.setCurrentMap(2);
        emptyAllObjects();
        hero.setWorldX(maxWorldCol - 2);
        hero.setWorldY(1);
    }

    /**
     * Empties all objects when switching maps.
     */
    private void emptyAllObjects() {
        logger.info("The first map objects were successfully reserved");
        Arrays.fill(items, null);
        Arrays.fill(monsters, null);
        setUpController.setUpObjects(items, tileController.getCurrentMap());
        setUpController.setUpMonsters(monsters, tileController.getCurrentMap());
        logger.info("The second map objects were successfully set up");
        npcElf = null;
    }

    /**
     * Adds all additional AnchorPanes to the main root pane.
     */
    private void addAnchorPanes() {
        root.getChildren().addAll(mapAnchorPane, itemsAnchorPane, entityAnchorPane, UIAnchorPane);
    }

    /**
     * Clears all secondary AnchorPanes.
     */
    private void clearAnchorPanes() {
        mapAnchorPane.getChildren().clear();
        itemsAnchorPane.getChildren().clear();
        entityAnchorPane.getChildren().clear();
        UIAnchorPane.getChildren().clear();
    }

    /**
     * Draws all components on the screen.
     */
    protected void paintComponent() {
        Platform.runLater(() -> {

            // first of all clean previous AnchorPanes
            clearAnchorPanes();

            // Map
            tileController.drawMap();

            // NPC
            drawNPC();

            // Monsters
            drawMonsters();

            // Player
            hero.getSpriteAnimation().drawHero(entityAnchorPane, hero.screenX, hero.screenY, hero.isDead());

            // Items
            drawItems();

            // UI
            userInterface.draw();
        });
    }


    private void drawNPC() {
        if (npcElf != null)
            npcElf.getSpriteAnimation().drawNPC(entityAnchorPane, getScreenX(npcElf.getWorldX() - (tileSize / 3)),
                    getScreenY(npcElf.getWorldY() + (tileSize / 3)));
    }


    private void drawMonsters() {
        for (int i = 0; i < monsters.length; i++) {
            if (monsters[i] != null)
                monsters[i].getSpriteAnimation().drawMonster(entityAnchorPane, getScreenX(monsters[i].getWorldX()),
                        getScreenY(monsters[i].getWorldY()), monsters[i].isDead());
        }
    }

    private void drawItems() {
        for (Items item : items) {
            if (item != null) {
                item.draw(this);
            }
        }
    }

    private int getScreenX(int worldX) {
        return worldX - hero.getWorldX() + hero.screenX;
    }

    private int getScreenY(int worldY) {
        return worldY - hero.getWorldY() + hero.screenY - tileSize;
    }

    /**
     * Sets up all game objects and monsters before the game starts.
     */
    protected void setUpGame() {
        logger.info("All game objects were set up");
        setUpController.setUpObjects(items, tileController.getCurrentMap());
        setUpController.setUpMonsters(monsters, tileController.getCurrentMap());
        turnOnMusic(Sounds.COMMON_SONG.getValue());
        gameState = States.PLAY_STATE;
    }

    protected void turnOnMusic(int i) {
        sound.setFile(i);
        sound.play();
        sound.loop();
    }

    protected void turnOffMusic() {
        sound.stop();
    }


    public void playClips(int i) {
        sound.setFile(i);
        sound.play();
    }

    /**
     * Displays a screen with the specified message, such as "game over" or "Victory".
     *
     * @param message the message to display
     */
    public void showScreen(String message) {
        Platform.runLater(() -> {

            switch (message) {
                case "game over":
                    logger.info("Hero died. Game over");
                    break;
                case "Victory":
                    logger.info("Hero opened a chest. Victory");
                    break;
            }

            clearAnchorPanes();

            AnchorPane gameOverPane = new AnchorPane();
            gameOverPane.setPrefSize(maxScreenWidth, maxScreenHeight);

            Text gameOverText = new Text(message);
            gameOverText.setFont(new Font(50));
            gameOverText.setFill(Color.RED);

            double textWidth = gameOverText.getLayoutBounds().getWidth();
            double textHeight = gameOverText.getLayoutBounds().getHeight();

            gameOverText.setX((maxScreenWidth - textWidth) / 2);
            gameOverText.setY((maxScreenHeight - textHeight) / 2 - 50);

            Button mainMenuButton = new Button("Return to Main Menu");
            mainMenuButton.setOnAction(event -> closeGameWindow());

            double buttonWidth = 200;

            mainMenuButton.setLayoutX((maxScreenWidth - buttonWidth) / 2);
            mainMenuButton.setLayoutY((maxScreenHeight + textHeight) / 2);

            gameOverPane.getChildren().addAll(gameOverText, mainMenuButton);

            root.getChildren().add(gameOverPane);
        });
    }

    /**
     * Closes the game window and stops the background music.
     */
    private void closeGameWindow() {
        turnOffMusic();
        Stage window = (Stage) root.getScene().getWindow();
        window.close();
    }

    // Getters and Setters
    public CollisionController getCollisionController() {
        return collisionController;
    }

    public Hero getHero() {
        return hero;
    }

    public Items[] getItems() {
        return items;
    }

    public AnchorPane getMapAnchorPane() {
        return mapAnchorPane;
    }

    public AnchorPane getItemsAnchorPane() {
        return itemsAnchorPane;
    }

    public NPCElf getNpcElf() {
        return npcElf;
    }

    public UserInterface getUserInterface() {
        return userInterface;
    }

    public AnchorPane getUIAnchorPane() {
        return UIAnchorPane;
    }

    public Monster[] getMonsters() {
        return monsters;
    }

    public void setGameState(States gameState) {
        this.gameState = gameState;
    }

}
