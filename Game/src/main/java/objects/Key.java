package objects;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class Key extends Items {
        protected int amount;
        public Key() {
            amount = 0;
            description = "Keys help you open doors or chests";
            name = "Key";
            try {
                imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                        getResourceAsStream("/items/key.png"))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }
    }