package main;

import entity.Monster;
import objects.*;

public class SetUpController {
    protected GameController gameController;

    public SetUpController(GameController gameController) {
        this.gameController = gameController;
    }

    /**
     * sets objects on maps
     */
    protected void setUpObjects(Items[] items, int currentMap) {
        // tests

        if (currentMap == 1) {
            // doors
            items[0] = new Door();
            items[0].setWorldX(11 * GameController.tileSize);
            items[0].setWorldY(15 * GameController.tileSize);

            items[1] = new Door();
            items[1].setWorldX(15 * GameController.tileSize);
            items[1].setWorldY(3 * GameController.tileSize);

            // swords
            items[2] = new Sword(1);
            items[2].setWorldX(GameController.tileSize);
            items[2].setWorldY(9 * GameController.tileSize);

            // keys
            items[3] = new Key();
            items[3].setWorldX(27 * GameController.tileSize);
            items[3].setWorldY(16 * GameController.tileSize);

            items[4] = new Key();
            items[4].setWorldX(28 * GameController.tileSize);
            items[4].setWorldY(GameController.tileSize);

            items[5] = new Key();
            items[5].setWorldX(11 * GameController.tileSize);
            items[5].setWorldY(GameController.tileSize);

            // boots
            items[6] = new Boots(2);
            items[6].setWorldX(GameController.tileSize);
            items[6].setWorldY(14 * GameController.tileSize);

            // heal poisons
            items[7] = new HealPoison(2);
            items[7].setWorldX(28 * GameController.tileSize);
            items[7].setWorldY(16 * GameController.tileSize);

        } else {

            // doors
            items[0] = new Door();
            items[0].setWorldX(19 * GameController.tileSize);
            items[0].setWorldY(10 * GameController.tileSize);

            items[1] = new Door();
            items[1].setWorldX(4 * GameController.tileSize);
            items[1].setWorldY(7 * GameController.tileSize);

            items[2] = new Door();
            items[2].setWorldX(8 * GameController.tileSize);
            items[2].setWorldY(16 * GameController.tileSize);

            // keys
            items[3] = new Key();
            items[3].setWorldX(28 * GameController.tileSize);
            items[3].setWorldY(23 * GameController.tileSize);

            items[4] = new Key();
            items[4].setWorldX(18 * GameController.tileSize);
            items[4].setWorldY(23 * GameController.tileSize);

            items[5] = new Key();
            items[5].setWorldX(GameController.tileSize);
            items[5].setWorldY(GameController.tileSize);

            items[9] = new Key();
            items[9].setWorldX(25 * GameController.tileSize);
            items[9].setWorldY(GameController.tileSize);

            // heal poisons
            items[6] = new HealPoison(2);
            items[6].setWorldX(18 * GameController.tileSize);
            items[6].setWorldY(GameController.tileSize);

            items[7] = new HealPoison(2);
            items[7].setWorldX(GameController.tileSize);
            items[7].setWorldY(23 * GameController.tileSize);

            // chest
            items[8] = new Chest();
            items[8].setWorldX(8 * GameController.tileSize);
            items[8].setWorldY(19 * GameController.tileSize);
        }
    }

    /**
     * sets up monsters on maps
     */
    protected void setUpMonsters(Monster[] monsters, int currentMap) {
        if (currentMap == 1) {
            monsters[0] = new Monster(GameController.tileSize * 5, GameController.tileSize * 19, gameController);
            monsters[1] = new Monster(GameController.tileSize * 8, GameController.tileSize * 21, gameController);
            monsters[2] = new Monster(GameController.tileSize * 12, GameController.tileSize * 20, gameController);
            monsters[3] = new Monster(GameController.tileSize * 21, GameController.tileSize * 5, gameController);
            monsters[4] = new Monster(GameController.tileSize * 25, GameController.tileSize * 6, gameController);
            monsters[5] = new Monster(GameController.tileSize * 8, GameController.tileSize * 21, gameController);
            monsters[6] = new Monster(GameController.tileSize * 8, GameController.tileSize * 21, gameController);
        } else {
            monsters[0] = new Monster(GameController.tileSize * 2, GameController.tileSize * 18, gameController);
            monsters[1] = new Monster(GameController.tileSize * 7, GameController.tileSize * 18, gameController);
            monsters[2] = new Monster(GameController.tileSize * 12, GameController.tileSize * 20, gameController);
            monsters[3] = new Monster(GameController.tileSize * 18, GameController.tileSize * 4, gameController);
            monsters[4] = new Monster(GameController.tileSize * 2, GameController.tileSize * 2, gameController);
            monsters[5] = new Monster(GameController.tileSize * 23, GameController.tileSize * 19, gameController);
            monsters[6] = new Monster(GameController.tileSize * 22, GameController.tileSize * 19, gameController);
        }
    }
}
