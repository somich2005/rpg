package tiles;

import main.GameController;

import java.util.logging.Logger;

public class TileController {

    private tile.TileController tileController;
    private static final Logger logger = Logger.getLogger(tile.TileController.class.getName());

    public static void main(String[] args) {
        TileController test = new TileController();
        test.setUp();
        test.testLoadMap();
    }

    public void setUp() {
        GameController gameController;
        gameController = new GameController();
        tileController = new tile.TileController(gameController);
    }

    public void testLoadMap() {
        int[][] map1 = new int[GameController.maxWorldCol][GameController.maxWorldRow];

        tileController.loadMap("/map/maps/Map1.txt", map1);
        if (map1 == null) {
            System.err.println("testLoadMap failed: Map1 array is null");
        } else if (map1.length == 0) {
            System.err.println("testLoadMap failed: Map1 array is empty");
        } else {
            boolean map1Loaded = false;
            for (int[] row : map1) {
                for (int tile : row) {
                    if (tile != 0) {
                        map1Loaded = true;
                        break;
                    }
                }
                if (map1Loaded) break;
            }
            if (map1Loaded) {
                System.out.println("testLoadMap for Map1 passed");
            } else {
                System.err.println("testLoadMap failed: Map1 array contains only zeros");
            }
        }

        int[][] map2 = new int[GameController.maxWorldCol][GameController.maxWorldRow];
        tileController.loadMap("/map/maps/Map2.txt", map2);
        if (map2 == null) {
            System.err.println("testLoadMap failed: Map2 array is null");
        } else if (map2.length == 0) {
            System.err.println("testLoadMap failed: Map2 array is empty");
        } else {
            boolean map2Loaded = false;
            for (int[] row : map2) {
                for (int tile : row) {
                    if (tile != 0) {
                        map2Loaded = true;
                        break;
                    }
                }
                if (map2Loaded) break;
            }
            if (map2Loaded) {
                logger.info("testLoadMap for Map2 passed");
            } else {
                logger.warning("testLoadMap failed: Map2 array contains only zeros");
            }
        }
    }
}
