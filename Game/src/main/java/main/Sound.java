package main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.net.URL;
import java.util.logging.Logger;

/**
 * The music class also almost copied from the tutorial
 * <a href="https://www.youtube.com/watch?v=nUHh_J2Acy8&list=PL_QPQmz5C6WUF-pOQDsbsKbaBZqXj4qSq&index=10&ab_channel=RyiSnow">...</a>
 */
public class Sound {

    protected boolean isPaused;
    protected Clip clip;
    protected URL[] soundsURL;
    private long clipTimePosition;

    // logger
    private static final Logger logger = Logger.getLogger(Sound.class.getName());

    public Sound() {
        setUpURL();
        isPaused = false;
    }

    // loads all sounds
    private void setUpURL() {
        soundsURL = new URL[10];
        soundsURL[0] = getClass().getResource("/sounds/music/song-1.wav"); // common song
        soundsURL[1] = getClass().getResource("/sounds/map/door-open.wav"); // door opening
        soundsURL[2] = getClass().getResource("/sounds/map/key-pick-up.wav"); // key pick up
        soundsURL[3] = getClass().getResource("/sounds/map/sword-pick-up.wav"); // sword pick up
        soundsURL[4] = getClass().getResource("/sounds/map/boots_put_on.wav"); // boots put on
        soundsURL[5] = getClass().getResource("/sounds/enemy/hitMonster.wav"); // Take a damage
        soundsURL[6] = getClass().getResource("/sounds/enemy/monsterdead.wav"); // Die
        soundsURL[7] = getClass().getResource("/sounds/map/poison.wav"); // Heal poison
        soundsURL[8] = getClass().getResource("/sounds/map/chest.wav"); // Chest
        logger.info("Every sound was loaded");
    }

    public void setFile(int i) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(soundsURL[i]); // opening audio files
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play() {
        clip.start();
        isPaused = false;
    }

    public void loop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        isPaused = false;
    }

    public void stop() {
        if (clip != null) {
            clip.stop();
            clip.close();
            isPaused = false;
        }
    }

    public void pause() {
        clipTimePosition = clip.getMicrosecondPosition();
        clip.stop();
        isPaused = true;
    }

    public void resume() {
        if (isPaused) {
            clip.setMicrosecondPosition(clipTimePosition);
            clip.start();
            isPaused = false;
        }
    }

    public boolean isPaused() {
        return isPaused;
    }
}
