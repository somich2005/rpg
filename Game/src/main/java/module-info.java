module org.example.game {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires jdk.compiler;
    requires java.logging;


    opens main to javafx.fxml;
    exports main;
    exports entity;
    opens entity to javafx.fxml;
}