package main;

/**
 * sounds enum
 */
public enum Sounds {
    COMMON_SONG(0),
    DOOR_OPENING(1),
    PICK_UP_KEY(2),
    PICK_UP_SWORD(3),
    PUT_ON_BOOTS(4),
    GET_DAMAGE(5),
    DIE(6),
    POISON(7),
    CHEST(8);

    private final int value;

    Sounds(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
