package entity;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import main.GameController;

import java.util.Objects;

public class SpriteAnimation {
    // Hero
    protected ImageView[] heroMoveLeft;
    protected ImageView[] heroMoveRight;
    protected ImageView[] heroIdleRight;
    protected ImageView[] heroIdleLeft;
    protected ImageView deadHero;

    // NPC elf
    protected ImageView[] elfIdle;

    // Monster
    protected ImageView[] zombieMoveRight;
    protected ImageView[] zombieMoveLeft;
    protected ImageView deadZombie;


    // counters and directions
    protected int spriteCounter;
    protected int spriteMoveNumber;
    protected Actions zombieAction;
    protected Actions heroAction;
    protected String heroDirection;
    protected String zombieDirection;

    public SpriteAnimation() {

        // counters and directions
        spriteCounter = 0;
        spriteMoveNumber = 0;
        heroDirection = "left";
        zombieDirection = "left";
        heroAction = Actions.RIGHT;
        zombieAction = Actions.RIGHT;
        initHero();
        initNPC();
        initZombie();
    }

    private void initHero() {
        // initialization
        heroMoveLeft = new ImageView[4];
        heroMoveRight = new ImageView[4];
        heroIdleRight = new ImageView[4];
        heroIdleLeft = new ImageView[4];
        deadHero = new ImageView(new Image(Objects.requireNonNull(getClass().
                getResourceAsStream("/Characters/knight/Dead/dead.png"))));

        // files strings
        String leftStr = "/Characters/knight/move/left/knight_f_run_anim_f%d.png";
        String rightStr = "/Characters/knight/move/right/knight_f_run_anim_f%d.png";
        String idleStrRight = "/Characters/knight/idle/knight_f_idle_anim_right_f%d.png";
        String idleStrLeft = "/Characters/knight/idle/knight_f_idle_anim_left_f%d.png";

        // setting
        setImages(leftStr, heroMoveLeft);
        setImages(rightStr, heroMoveRight);
        setImages(idleStrRight, heroIdleRight);
        setImages(idleStrLeft, heroIdleLeft);
    }

    private void initNPC() {
        // initialization

        elfIdle = new ImageView[4];

        // files strings
        String npcIdle = "/NPC/elf/elf_f_idle_anim_f%d.png";

        // setting
        setImages(npcIdle, elfIdle);
    }

    private void initZombie() {
        // initialization
        zombieMoveRight = new ImageView[4];
        zombieMoveLeft = new ImageView[4];
        deadZombie = new ImageView(new Image(Objects.requireNonNull(getClass().
                getResourceAsStream("/monsters/zombie/dead/dead.png"))));

        // files strings
        String rightStr = "/monsters/zombie/move/right/big_zombie_run_anim_f%d.png";
        String leftStr = "/monsters/zombie/move/left/big_zombie_run_anim_f%d.png";

        //setting

        setImages(rightStr, zombieMoveRight);
        setImages(leftStr, zombieMoveLeft);
    }


    private void setImages(String formatedString, ImageView[] images) { // sets any image to the array
        String path;
        Image image;
        for (int i = 0; i < 4; i++) { // get the move images
            // format strings
            path = String.format(formatedString, i);
            // get images
            image = new Image(Objects.requireNonNull(getClass().getResourceAsStream(path)));
            // fill the arrays
            images[i] = new ImageView(image);
        }
    }

    // draws hero
    public void drawHero(AnchorPane root, int positionX, int positionY,  boolean isDead) {
        Platform.runLater(() -> {
            ImageView hero = null;
            if (!isDead) {
                switch (heroAction) {
                    case Actions.DOWN, Actions.UP:
                        hero = heroDirection.equals("right") ? heroMoveRight[spriteMoveNumber] : heroMoveLeft[spriteMoveNumber];
                        break;
                    case Actions.LEFT:
                        hero = heroMoveLeft[spriteMoveNumber];
                        break;
                    case Actions.RIGHT:
                        hero = heroMoveRight[spriteMoveNumber];
                        break;
                    case Actions.IDLE:
                        hero = heroDirection.equals("left") ? heroIdleLeft[spriteMoveNumber] : heroIdleRight[spriteMoveNumber];
                        break;
                }
                if (hero != null) {
                    addEntityToRoot(root, hero, positionX, positionY, 1, 1);
                }
            } else {
                addEntityToRoot(root, deadHero, positionX, positionY, 1, 1);
            }
        });
    }

    //draws npc
    public void drawNPC(AnchorPane root, int positionX, int positionY) {
        Platform.runLater(() -> {
            ImageView npc = elfIdle[spriteMoveNumber];
            if (npc != null) {
                addEntityToRoot(root, npc, positionX, positionY, 1,  1.5);
            }
        });
    }

    // draws monster
    public void drawMonster(AnchorPane root, int positionX, int positionY, boolean isDead) {
        Platform.runLater(() -> {
            if (isDead) {
                addEntityToRoot(root, deadZombie, positionX, positionY, 1.5, 1.5);
            } else {
                ImageView zombie = null;
                switch (zombieAction) {
                    case Actions.DOWN, Actions.UP:
                        zombie = zombieDirection.equals("right") ? zombieMoveRight[spriteMoveNumber] : zombieMoveLeft[spriteMoveNumber];
                        break;
                    case Actions.LEFT:
                        zombie = zombieMoveLeft[spriteMoveNumber];
                        break;
                    case Actions.RIGHT:
                        zombie = zombieMoveRight[spriteMoveNumber];
                        break;
                }
                addEntityToRoot(root, zombie, positionX, positionY, 1.5, 1.5);
            }
        });
    }

    //adds any image to the AnchorPane
    private void addEntityToRoot(AnchorPane root, ImageView imageView, int x, int y, double width, double height) {
        if (imageView != null) {
            imageView.setFitWidth(GameController.tileSize * width);
            imageView.setFitHeight(GameController.tileSize * height);
            imageView.setX(x);
            imageView.setY(y);
            if (!root.getChildren().contains(imageView)) {
                root.getChildren().add(imageView);
            }
        }
    }


    // getters

    public Actions getHeroAction() {
        return heroAction;
    }

    public Actions getZombieAction() {
        return zombieAction;
    }

    public String getZombieDirection() {
        return zombieDirection;
    }
}