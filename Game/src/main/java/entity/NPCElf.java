package entity;

import javafx.scene.shape.Rectangle;
import main.GameController;

import java.util.logging.Logger;

public class NPCElf extends Entity {

    // dialogs
    protected String[] dialogs;
    protected int dialogI;

    public NPCElf(int worldX, int worldY, GameController gameController) {
        super(worldX, worldY, gameController);

        // dialogs
        dialogs = new String[10];
        setUpDialogues();
    }

    public void updateNPC() {
        spriteAnimation.spriteCounter++;

        if (spriteAnimation.spriteCounter > 5) {
            spriteAnimation.spriteMoveNumber++;
            if (spriteAnimation.spriteMoveNumber == 4)
                spriteAnimation.spriteMoveNumber = 0;
            spriteAnimation.spriteCounter = 0;
        }
        setSolidArea();
    }

    protected void setUpDialogues() {
        dialogI = 0;
        dialogs[0] = "Greetings, brave hero! I am Lorendil, an elf and guardian of this ancient dungeon filled with monsters, treasures, and ancient knowledge. " +
                "Fight your way through goblins, spiders, and golems to reach the mighty dragon guarding the main artifact.";
        dialogs[1] = "Farewell, hero. Go forth and conquer the darkness."; // default message
    }

    public void say() {
        gameController.getUserInterface().setDialogString(dialogs[dialogI]);
        if (dialogI < 1)
            dialogI++;
        Logger.getLogger(NPCElf.class.getName()).info("Current game state - dialog");
    }

    @Override
    protected void setSolidArea() {
        solidArea = new Rectangle();
        solidArea.setWidth((double) GameController.tileSize - 12);
        solidArea.setHeight((double) GameController.tileSize - 12);
    }
}
