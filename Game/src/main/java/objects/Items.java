package objects;

import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import main.GameController;

/**
 * The Items class represents game items that can be placed on the map.
 * It handles the position, appearance, and collision properties of the items.
 */
public class Items {
    // Position
    protected int worldX;
    protected int worldY;
    protected int solidAreaDefaultX;
    protected int solidAreaDefaultY;

    // Main
    protected String name;
    protected boolean collision;
    protected ImageView imageView;
    protected Rectangle solidArea;
    protected String description;


    public Items() {
        // Main
        setSolidArea();

        // Position
        solidAreaDefaultX = (int) solidArea.getX();
        solidAreaDefaultY = (int) solidArea.getY();
    }

    /**
     * sets the solid area for collision detection.
     */
    private void setSolidArea() {
        solidArea = new Rectangle();
        solidArea.setWidth(GameController.tileSize);
        solidArea.setHeight(GameController.tileSize);
    }

    /**
     * draws the item on the map if it is within the visible area.
     *
     * @param gameController the GameController instance
     */
    public void draw(GameController gameController) {
        int screenX = worldX - gameController.getHero().getWorldX() + gameController.getHero().screenX;
        int screenY = worldY - gameController.getHero().getWorldY() + gameController.getHero().screenY;

        // Draw only visible tiles
        if (worldX + GameController.tileSize > gameController.getHero().getWorldX() - gameController.getHero().screenX &&
                worldX - GameController.tileSize < gameController.getHero().getWorldX() + gameController.getHero().screenX &&
                worldY + GameController.tileSize > gameController.getHero().getWorldY() - gameController.getHero().screenY &&
                worldY - GameController.tileSize < gameController.getHero().getWorldY() + gameController.getHero().screenY) {
            addItemToRoot(screenX, screenY, imageView, gameController.getItemsAnchorPane());
        }
    }

    /**
     * adds the item to the root pane at the specified position.
     */
    protected void addItemToRoot(int x, int y, ImageView imageView, AnchorPane root) {
        ImageView imageViewTmp = new ImageView(imageView.getImage());
        imageViewTmp.setX(x);
        imageViewTmp.setY(y);
        imageViewTmp.setFitHeight(GameController.tileSize);
        imageViewTmp.setFitWidth(GameController.tileSize);
        root.getChildren().add(imageViewTmp);
    }

    // Getters

    public Rectangle getSolidArea() {
        return solidArea;
    }

    public int getWorldX() {
        return worldX;
    }


    public int getWorldY() {
        return worldY;
    }

    public int getSolidAreaDefaultX() {
        return solidAreaDefaultX;
    }

    public int getSolidAreaDefaultY() {
        return solidAreaDefaultY;
    }

    /**
     * Checks if the item has collision enabled.
     *
     * @return true if the item has collision enabled, false otherwise
     */
    public boolean isCollision() {
        return collision;
    }

    public String getName() {
        return name;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public String getDescription() {
        return description;
    }

    // Setters

    public void setWorldY(int worldY) {
        this.worldY = worldY;
    }

    public void setWorldX(int worldX) {
        this.worldX = worldX;
    }
}
