package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class Boots extends Items {
    protected int additionalSpeed;

    public Boots(int additionalSpeed) {
        this.additionalSpeed = additionalSpeed;
        description = String.format("Boots make you faster. Additional speed: %d", additionalSpeed);
        name = "Boots";
        try {
            imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                    getResourceAsStream("/items/iron_boots.png"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}