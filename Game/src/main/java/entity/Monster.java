package entity;

import javafx.scene.shape.Rectangle;
import main.GameController;
import main.Sounds;
import objects.Health;

import java.util.Random;

public class Monster extends Entity {

    private final Random random;
    protected boolean invincible;
    protected int invincibleCounter;


    public Monster(int positionX, int positionY, GameController gameController) {
        super(positionX, positionY, gameController);

        // game properties
        damage = 1;
        speed = 4;
        setSolidArea();
        health = new Health(3);


        // random
        random = new Random();

        // invincibility
        invincible = false;
        invincibleCounter = 0;
    }

    @Override
    protected void setSolidArea() {
        solidArea = new Rectangle(); // set collision parameters a little bit smaller than a tilesize
        solidArea.setWidth(GameController.tileSize);
        solidArea.setHeight(GameController.tileSize);
        solidArea.setY(-24);
        solidAreaDefaultX = (int) solidArea.getX();
        solidAreaDefaultY = (int) solidArea.getY();
    }

    protected void setAction() { // simple "AI"
        actionLockCounter++;  // monster moves randomly
        if (actionLockCounter == 120) {
            int i = random.nextInt(100) + 1;
            if (i <= 25) {
                spriteAnimation.zombieAction = Actions.UP;
            }
            if (i > 25 && i <= 50) {
                spriteAnimation.zombieAction = Actions.DOWN;
            }
            if (i > 50 && i <= 75) {
                spriteAnimation.zombieAction = Actions.LEFT;
                spriteAnimation.zombieDirection = "left";
            }
            if (i > 75) {
                spriteAnimation.zombieAction = Actions.RIGHT;
                spriteAnimation.zombieDirection = "right";
            }
            actionLockCounter = 0;
        }
    }

    public void update() {
        if (!isDead) {
            setAction();
            collisionOn = true;
            gameController.getCollisionController().checkTile(this, spriteAnimation.zombieAction);
            gameController.getCollisionController().checkObject(this, false);
            gameController.getCollisionController().checkEntity(this, gameController.getNpcElf(), false);
            boolean contact = gameController.getCollisionController().checkPlayer(this);
            if (contact && !gameController.getHero().invincible) {
                gameController.getHero().health.setCurrentHealth(gameController.getHero().health.getCurrentHealth() - 1);
                gameController.getHero().invincible = true;
            }
            if (!collisionOn) {
                switch (spriteAnimation.zombieAction) {
                    case Actions.UP:
                        moveUp();
                        break;
                    case Actions.DOWN:
                        moveDown();
                        break;
                    case Actions.LEFT:
                        moveLeft();
                        break;
                    case Actions.RIGHT:
                        moveRight();
                        break;
                }
            }
            updateAnimation();
        }
    }

    private void updateAnimation() {
        spriteAnimation.spriteCounter++;
        if (spriteAnimation.spriteCounter > 5) { // moves every 5 frames as a hero
            spriteAnimation.spriteMoveNumber++;
            if (spriteAnimation.spriteMoveNumber == 4)
                spriteAnimation.spriteMoveNumber = 0;
            spriteAnimation.spriteCounter = 0;
        }
        if (invincible) {
            invincibleCounter++;
            if (invincibleCounter > 20) {
                invincible = false;
                invincibleCounter = 0;
            }
        }
    }

    @Override
    protected void die() {
        gameController.playClips(Sounds.DIE.getValue());
        solidArea = null;
        speed = 0;
        isDead = true;
    }

}
