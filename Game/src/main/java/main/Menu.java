package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * The Menu class is the main entry point for the JavaFX application.
 */
public class Menu extends Application {

    private static final String menuBackgroundImage = "/images/MenuBackground.png";
    private static final Logger logger = Logger.getLogger(Menu.class.getName());

    /**
     * Starts the JavaFX application.
     *
     * @param stage the primary stage for this application
     * @throws IOException if the FXML file cannot be loaded
     */
    @Override
    public void start(Stage stage) throws IOException {
        logger.info("Starting the menu application");
        FXMLLoader fxmlLoader = new FXMLLoader(Menu.class.getResource("/fxml_Files/Menu.fxml"));
        Pane root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setTitle("Menu");
        setBackgroundImage(root, menuBackgroundImage);
        stage.setScene(scene);
        stage.show();
    }

    public static void setBackgroundImage(Pane root, String path) {
        URL backgroundImageUrl = Menu.class.getResource(path);
        Image backgroundImage = new Image(backgroundImageUrl.toExternalForm());

        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        root.setBackground(new Background(background));
    }

    public static void main(String[] args) {
        logger.info("launching the application");
        launch(args);
    }
}
