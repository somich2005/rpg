package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import main.GameController;

import java.util.Objects;

public class Sword extends Items {

    protected int additionalDamage;

    public Sword(int additionalDamage) {
        this.additionalDamage = additionalDamage;
        description = String.format("increase your damage. Additional damage: %d", additionalDamage);
        name = "Sword";
        try {
            imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                    getResourceAsStream("/items/weapon_regular_sword.png"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void addItemToRoot(int x, int y, ImageView imageView, AnchorPane root) {
        ImageView imageViewTmp = new ImageView(imageView.getImage());
        imageViewTmp.setX(x);
        imageViewTmp.setY(y);
        imageViewTmp.setFitHeight(GameController.tileSize);
        imageViewTmp.setFitWidth((double) GameController.tileSize / 2); // divide by 2 because item looks bad
        root.getChildren().add(imageViewTmp);
    }

    // getters

    public int getAdditionalDamage() {
        return additionalDamage;
    }
}
