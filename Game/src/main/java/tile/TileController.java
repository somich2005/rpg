package tile;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import main.GameController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * The TileController class is responsible for managing and rendering tiles on the game map.
 * It loads tile images, handles map data, and draws the map based on the current game state.
 */
public class TileController {

    // Tiles
    protected Tile[] tiles;

    // Maps
    protected int[][] mapTileNum1;
    protected int[][] mapTileNum2;
    protected int currentMap;

    // Main
    protected GameController gameController;

    public TileController(GameController gameController) {
        // Tiles
        tiles = new Tile[41];
        loadTileImages();
        mapTileNum1 = new int[GameController.maxWorldCol][GameController.maxWorldRow];
        mapTileNum2 = new int[GameController.maxWorldCol][GameController.maxWorldRow];

        // Maps
        loadMap("/map/maps/Map1.txt", mapTileNum1);
        loadMap("/map/maps/Map2.txt", mapTileNum2);
        Logger.getLogger(TileController.class.getName()).info("All maps were loaded successfully");
        currentMap = 1;

        // Main
        this.gameController = gameController;
    }

    /**
     * Loads tile images from the specified paths.
     * Sets collision for non-floor tiles.
     */

    public void loadTileImages() {
        String[] imagePaths = {
                "doors_frame_left.png", "doors_frame_right.png", "doors_leaf_closed.png",
                "doors_leaf_open.png", "doors_frame_top.png", "floor_1.png", "floor_2.png",
                "floor_3.png", "floor_4.png", "floor_5.png", "floor_6.png", "floor_7.png",
                "floor_8.png", "wall_edge_bottom_left.png", "wall_edge_bottom_right.png",
                "wall_edge_left.png", "wall_edge_mid_left.png", "wall_edge_mid_right.png",
                "wall_edge_right.png", "wall_edge_top_left.png", "wall_edge_top_right.png",
                "wall_edge_tshape_bottom_left.png", "wall_edge_tshape_bottom_right.png",
                "wall_edge_tshape_left.png", "wall_edge_tshape_right.png", "wall_fountain_top_1.png",
                "wall_fountain_top_2.png", "wall_fountain_top_3.png", "wall_hole_1.png",
                "wall_left.png", "wall_mid.png", "wall_outer_front_left.png",
                "wall_outer_front_right.png", "wall_outer_mid_left.png", "wall_outer_mid_right.png",
                "wall_outer_top_left.png", "wall_outer_top_right.png", "wall_right.png",
                "wall_top_left.png", "wall_top_mid.png", "wall_top_right.png", "floor_ladder.png"
        };
        tiles = new Tile[imagePaths.length];
        try {
            for (int i = 0; i < imagePaths.length; i++) {
                tiles[i] = new Tile();
                tiles[i].imageView = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/map/objects/" + imagePaths[i]))));
                if ((i < 5 || i > 12) && i != 41) // Set collision to everything except floor
                    tiles[i].collision = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * draws the map based on the current state and visibility of tiles.
     */
    public void drawMap() {
        Platform.runLater(() -> {
            AnchorPane root = gameController.getMapAnchorPane();
            int wCol = 0;
            int wRow = 0;

            while (wCol <= GameController.maxWorldCol && wRow < GameController.maxWorldRow) {
                int tileNum;
                if (currentMap == 1)
                    tileNum = mapTileNum1[wCol][wRow];
                else
                    tileNum = mapTileNum2[wCol][wRow];

                int worldX = wCol * GameController.tileSize;
                int worldY = wRow * GameController.tileSize;
                int screenX = worldX - gameController.getHero().getWorldX() + gameController.getHero().screenX;
                int screenY = worldY - gameController.getHero().getWorldY() + gameController.getHero().screenY;

                // Draw only visible tiles
                if (worldX + GameController.tileSize > gameController.getHero().getWorldX() - gameController.getHero().screenX &&
                        worldX - GameController.tileSize < gameController.getHero().getWorldX() + gameController.getHero().screenX &&
                        worldY + GameController.tileSize > gameController.getHero().getWorldY() - gameController.getHero().screenY &&
                        worldY - GameController.tileSize < gameController.getHero().getWorldY() + gameController.getHero().screenY)
                    drawImage(screenX, screenY, tiles[tileNum].imageView, root);
                wCol++;
                if (wCol == GameController.maxWorldCol) {
                    wCol = 0;
                    wRow++;
                }
            }
        });
    }

    private void drawImage(int x, int y, ImageView imageView, AnchorPane root) {
        ImageView imageViewTmp = new ImageView(imageView.getImage());
        imageViewTmp.setX(x);
        imageViewTmp.setY(y);
        imageViewTmp.setFitHeight(GameController.tileSize);
        imageViewTmp.setFitWidth(GameController.tileSize);
        root.getChildren().add(imageViewTmp);
    }

    /**
     * Loads a map from a .txt file into the specified map array.
     *
     * @param mapArr the map array to load the data into
     */


    public void loadMap(String path, int[][] mapArr) {
        try {
            InputStream inputStream = getClass().getResourceAsStream(path);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            int row = 0;
            String line;
            while ((line = bufferedReader.readLine()) != null && row < GameController.maxWorldRow) {
                String[] numbers = line.split(" {2}");
                for (int col = 0; col < numbers.length && col < GameController.maxWorldCol; col++) {
                    int num = Integer.parseInt(numbers[col]);
                    mapArr[col][row] = num;
                }
                row++;
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Tile[] getTiles() {
        return tiles;
    }

    // Getters

    public int getCurrentMap() {
        return currentMap;
    }

    public int[][] getCurrentMapTileNum() {
        if (currentMap == 1)
            return mapTileNum1;
        else
            return mapTileNum2;
    }

    // Setters

    public void setCurrentMap(int currentMap) {
        this.currentMap = currentMap;
    }
}
