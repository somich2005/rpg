package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


import java.util.Objects;


public class Health extends Items {
    protected int maxHealth;
    protected int currentHealth;
    protected ImageView fullHeart;
    protected ImageView halfHeart;
    protected ImageView emptyHeart;


    public Health(int maxHealth) {
        this.maxHealth = maxHealth;
        currentHealth = maxHealth; // hp bar is full at rhe start of the game
        name = "Health";
        try {
            getHearts();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getHearts() {
        fullHeart = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Characters/knight/lifes/ui_heart_full.png"))));
        halfHeart = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Characters/knight/lifes/ui_heart_half.png"))));
        emptyHeart = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Characters/knight/lifes/ui_heart_empty.png"))));
    }

    // getters
    public ImageView[] getHeartImages() {
        return new ImageView[]{fullHeart, halfHeart, emptyHeart};
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    // setters
    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }
}
