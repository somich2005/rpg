package main;

/**
 * game states enum
 */
public enum States {
    PLAY_STATE(1),
    PAUSE_STATE(2),
    DIALOG_STATE(3),
    INVENTORY_STATE(4);

    private final int value;

    States(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
