package entity;

import javafx.scene.shape.Rectangle;
import main.GameController;
import objects.Health;

public class Entity {

    // position
    protected int worldX;
    protected int worldY;
    protected Rectangle solidArea;

    // game properties
    protected int speed;
    protected int solidAreaDefaultX;
    protected int solidAreaDefaultY;
    protected Health health;
    protected int damage;
    protected boolean isDead;

    // main
    protected boolean collisionOn = false;
    protected GameController gameController;
    protected SpriteAnimation spriteAnimation;
    protected int actionLockCounter;

    public Entity(int worldX, int worldY, GameController gameController) {
        // position
        this.gameController = gameController;
        this.worldX = worldX;
        this.worldY = worldY;
        setSolidArea();

        // game properties
        isDead = false;

        //main
        spriteAnimation = new SpriteAnimation();
        actionLockCounter = 0;
    }

    protected void setSolidArea() { // setting a solidArea for collision
        solidArea = new Rectangle(); // set collision parameters a little bit smaller than a tilesize
        solidArea.setX(16);
        solidArea.setY(16);
        solidArea.setWidth(25);
        solidArea.setHeight(25);
    }

    protected void die(){
        isDead = true;
    }

    public void moveUp() {
        worldY -= speed;
    }

    public void moveDown() {
        worldY += speed;
    }

    public void moveLeft() {
        worldX -= speed;
    }

    public void moveRight() {
        worldX += speed;
    }

    public int getWorldX() {
        return worldX;
    }

    public int getWorldY() {
        return worldY;
    }


    public Rectangle getSolidArea() {
        return solidArea;
    }

    public int getSpeed() {
        return speed;
    }

    public void setCollisionOn(boolean collisionOn) {
        this.collisionOn = collisionOn;
    }

    public int getSolidAreaDefaultY() {
        return solidAreaDefaultY;
    }

    public int getSolidAreaDefaultX() {
        return solidAreaDefaultX;
    }

    public SpriteAnimation getSpriteAnimation() {
        return spriteAnimation;
    }

    public Health getHealth() {
        return health;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setWorldX(int worldX) {
        this.worldX = worldX;
    }

    public void setWorldY(int worldY) {
        this.worldY = worldY;
    }

    public int getDamage() {
        return damage;
    }
}
