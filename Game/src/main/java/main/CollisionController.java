package main;

import entity.Entity;
import javafx.scene.shape.Rectangle;
import entity.Actions;

/**
 * The CollisionController class is responsible for handling collision detection
 * between entities, tiles, and objects in the game.
 */
public class CollisionController {
    protected GameController gameController;
    
    public CollisionController(GameController gameController) {
        this.gameController = gameController;
    }

    /**
     * Checks for tile collisions based on the entity's current action.
     *
     * @param entity the entity to check for collisions
     * @param action the action being performed by the entity
     */
    public void checkTile(Entity entity, Actions action) {
        entity.setCollisionOn(false);

        int entityLeftWorldX = entity.getWorldX() + (int) entity.getSolidArea().getX();
        int entityRightWorldX = entity.getWorldX() + (int) entity.getSolidArea().getX() + (int) entity.getSolidArea().getWidth();
        int entityTopWorldY = entity.getWorldY() + (int) entity.getSolidArea().getY();
        int entityBotWorldY = entity.getWorldY() + (int) entity.getSolidArea().getY() + (int) entity.getSolidArea().getHeight();

        int entityLeftCol = entityLeftWorldX / GameController.tileSize;
        int entityRightCol = entityRightWorldX / GameController.tileSize;
        int entityTopRow = entityTopWorldY / GameController.tileSize;
        int entityBotRow = entityBotWorldY / GameController.tileSize;

        int tileNumber1;
        int tileNumber2;

        switch (action) {
            case Actions.UP:
                entityTopRow = (entityTopWorldY - entity.getSpeed()) / GameController.tileSize;
                tileNumber1 = gameController.tileController.getCurrentMapTileNum()[entityLeftCol][entityTopRow];
                tileNumber2 = gameController.tileController.getCurrentMapTileNum()[entityRightCol][entityTopRow];
                if (gameController.tileController.getTiles()[tileNumber1].isCollision() ||
                        gameController.tileController.getTiles()[tileNumber2].isCollision()) {
                    entity.setCollisionOn(true);
                }
                break;
            case Actions.DOWN:
                entityBotRow = (entityBotWorldY + entity.getSpeed()) / GameController.tileSize;
                tileNumber1 = gameController.tileController.getCurrentMapTileNum()[entityLeftCol][entityBotRow];
                tileNumber2 = gameController.tileController.getCurrentMapTileNum()[entityRightCol][entityBotRow];
                if (gameController.tileController.getTiles()[tileNumber1].isCollision() ||
                        gameController.tileController.getTiles()[tileNumber2].isCollision()) {
                    entity.setCollisionOn(true);
                }
                break;
            case Actions.LEFT:
                entityLeftCol = (entityLeftWorldX - entity.getSpeed()) / GameController.tileSize;
                tileNumber1 = gameController.tileController.getCurrentMapTileNum()[entityLeftCol][entityTopRow];
                tileNumber2 = gameController.tileController.getCurrentMapTileNum()[entityLeftCol][entityBotRow];
                if (gameController.tileController.getTiles()[tileNumber1].isCollision() ||
                        gameController.tileController.getTiles()[tileNumber2].isCollision()) {
                    entity.setCollisionOn(true);
                }
                break;
            case Actions.RIGHT:
                entityRightCol = (entityRightWorldX + entity.getSpeed()) / GameController.tileSize;
                tileNumber1 = gameController.tileController.getCurrentMapTileNum()[entityRightCol][entityTopRow];
                tileNumber2 = gameController.tileController.getCurrentMapTileNum()[entityRightCol][entityBotRow];
                if (gameController.tileController.getTiles()[tileNumber1].isCollision() ||
                        gameController.tileController.getTiles()[tileNumber2].isCollision()) {
                    entity.setCollisionOn(true);
                }
                break;
        }
    }

    /**
     * Checks if an entity collides with any objects.
     *
     * @param entity the entity to check for collisions
     * @param isPlayer a boolean indicating if the entity is the player
     * @return the index of the object being collided with, or -1 if no collision
     */
    public int checkObject(Entity entity, boolean isPlayer) {
        int index = -1;

        for (int i = 0; i < gameController.getItems().length; i++) {
            if (gameController.getItems()[i] != null) {
                Rectangle entitySolidArea = entity.getSolidArea();
                entitySolidArea.setX(entity.getWorldX() + entitySolidArea.getX());
                entitySolidArea.setY(entity.getWorldY() + entitySolidArea.getY());

                Rectangle itemSolidArea = gameController.getItems()[i].getSolidArea();
                itemSolidArea.setX(gameController.getItems()[i].getWorldX() + itemSolidArea.getX());
                itemSolidArea.setY(gameController.getItems()[i].getWorldY() + itemSolidArea.getY());

                moveSolidArea(entity, entitySolidArea);

                if (entitySolidArea.intersects(itemSolidArea.getBoundsInLocal())) {
                    if (gameController.getItems()[i].isCollision()) {
                        entity.setCollisionOn(true);
                    }
                    if (isPlayer) {
                        index = i;
                    }
                }

                entitySolidArea.setX(entity.getSolidAreaDefaultX());
                entitySolidArea.setY(entity.getSolidAreaDefaultY());
                itemSolidArea.setX(gameController.getItems()[i].getSolidAreaDefaultX());
                itemSolidArea.setY(gameController.getItems()[i].getSolidAreaDefaultY());
            }
        }
        return index;
    }


    private void moveSolidArea(Entity entity, Rectangle entitySolidArea) {
        switch (gameController.getHero().getSpriteAnimation().getHeroAction()) {
            case Actions.UP:
                entitySolidArea.setY(entitySolidArea.getY() - entity.getSpeed());
                break;
            case Actions.DOWN:
                entitySolidArea.setY(entitySolidArea.getY() + entity.getSpeed());
                break;
            case Actions.LEFT:
                entitySolidArea.setX(entitySolidArea.getX() - entity.getSpeed());
                break;
            case Actions.RIGHT:
                entitySolidArea.setX(entitySolidArea.getX() + entity.getSpeed());
                break;
        }
    }

    /**
     * Checks for collisions between a source entity and a target entity (e.g., NPC or monster).
     *
     * @param src    the source entity
     * @param target the target entity
     * @param isDead boolean indicating if the target entity is dead
     * @return 1 if there is an interaction with the target, -1 otherwise
     */
    public int checkEntity(Entity src, Entity target, boolean isDead) {
        int index = -1;
        if (!isDead && target != null) {
            Rectangle entitySolidArea = src.getSolidArea();
            entitySolidArea.setX(src.getWorldX() + entitySolidArea.getX());
            entitySolidArea.setY(src.getWorldY() + entitySolidArea.getY());

            Rectangle itemSolidArea = target.getSolidArea();
            itemSolidArea.setX(target.getWorldX() + itemSolidArea.getX());
            itemSolidArea.setY(target.getWorldY() + itemSolidArea.getY());

            moveSolidArea(src, entitySolidArea);

            if (entitySolidArea.intersects(itemSolidArea.getBoundsInLocal())) {
                src.setCollisionOn(true);
                index = 1;
            }
            entitySolidArea.setX(src.getSolidAreaDefaultX());
            entitySolidArea.setY(src.getSolidAreaDefaultY());
            itemSolidArea.setX(target.getSolidAreaDefaultX());
            itemSolidArea.setY(target.getSolidAreaDefaultY());
        }
        return index;
    }

    /**
     * Checks if a monster collides with the player.
     *
     * @param entity the monster entity
     * @return true if there is a collision with the player, false otherwise
     */
    public boolean checkPlayer(Entity entity) {
        Rectangle entitySolidArea = entity.getSolidArea();
        entitySolidArea.setX(entity.getWorldX() + entitySolidArea.getX());
        entitySolidArea.setY(entity.getWorldY() + entitySolidArea.getY());

        Rectangle playerSolidArea = gameController.getHero().getSolidArea();
        playerSolidArea.setX(gameController.getHero().getWorldX() + playerSolidArea.getX());
        playerSolidArea.setY(gameController.getHero().getWorldY() + playerSolidArea.getY());

        boolean collisionDetected = false;

        switch (entity.getSpriteAnimation().getZombieAction()) {
            case Actions.UP:
                entitySolidArea.setY(entitySolidArea.getY() - entity.getSpeed());
                break;
            case Actions.DOWN:
                entitySolidArea.setY(entitySolidArea.getY() + entity.getSpeed());
                break;
            case Actions.LEFT:
                entitySolidArea.setX(entitySolidArea.getX() - entity.getSpeed());
                break;
            case Actions.RIGHT:
                entitySolidArea.setX(entitySolidArea.getX() + entity.getSpeed());
                break;
        }

        if (entitySolidArea.intersects(playerSolidArea.getBoundsInLocal())) {
            entity.setCollisionOn(true);
            collisionDetected = true;
        }
        entitySolidArea.setX(entity.getSolidAreaDefaultX());
        entitySolidArea.setY(entity.getSolidAreaDefaultY());
        playerSolidArea.setX(gameController.getHero().getSolidAreaDefaultX());
        playerSolidArea.setY(gameController.getHero().getSolidAreaDefaultY());

        if (collisionDetected) {
            switch (entity.getSpriteAnimation().getZombieAction()) {
                case Actions.UP:
                    entity.moveDown();
                    break;
                case Actions.DOWN:
                    entity.moveUp();
                    break;
                case Actions.LEFT:
                    entity.moveRight();
                    break;
                case Actions.RIGHT:
                    entity.moveLeft();
                    break;
            }
        }
        return collisionDetected;
    }
}
