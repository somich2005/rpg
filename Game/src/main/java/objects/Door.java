package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class Door extends Items {

    protected boolean isOpen;

    public Door() {
        isOpen = false;
        name = "Door";
        try {
            collision = true;
            imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                    getResourceAsStream("/map/objects/doors_leaf_closed.png"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
