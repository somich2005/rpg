package objects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

public class HealPoison extends Items {

    protected int healing;

    public HealPoison(int healing) {
        this.healing = healing;
        description = "Poisons heal you. Healing: +1hp";
        name = "Heal poison";
        try {
            imageView = new ImageView(new Image(Objects.requireNonNull(getClass().
                    getResourceAsStream("/items/flask_red.png"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getHealing() {
        return healing;
    }
}
