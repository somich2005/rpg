package main;

import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Game {
    protected Scene scene;
    protected Stage stage;
    protected   AnchorPane root;
    GameController gameController;


    public Game() {
        stage = new Stage();
        stage.setWidth(GameController.maxScreenWidth);
        stage.setHeight((double) GameController.maxScreenHeight + 35);
        stage.setTitle("Game");
        root = new AnchorPane();
        root.setStyle("-fx-background-color: grey;");
        scene = new Scene(root);
        gameController = new GameController(root, this.stage, scene);
    }

    public void show() {
        stage.setScene(scene);
        gameController.setUpGame();
        gameController.startGameThread();
        stage.show();
    }
}
